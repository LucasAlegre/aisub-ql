import interfaces as controller_template
from itertools import product
from typing import Tuple, List, Any
import random
from math import sqrt, exp

primes = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101,
          103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193)

ACTION_UP = 1
ACTION_DOWN = 2
ACTIONS = (ACTION_UP, ACTION_DOWN)
AHEAD = 0
UP = 1
UPR = 2
DOWN = 3
DOWNR = 4
WATER_DIS = 5
WATER = 6
DISTANCES = (0, 1, 2, 3, 4)
STEPS_WATER = 0


class State(controller_template.State):
    def __init__(self, sensors: list):
        self.sensors = sensors

    def compute_features(self) -> Tuple:
        """
        This function should take the raw sensor information of the submarine (see below) and compute useful features for selecting an action
        The submarine has the following sensors:

        self.sensors contains (in order):
        
        0 water_UP: 1-700
        1 water_UP_RIGHT: 1-700
        2 obstacle_UP: 1-700
        3 obstacle_UP_RIGHT: 1-700
        4 obstacle_AHEAD: 1-700
        5 obstacle_DOWN_RIGHT: 1-700
        6 obstacle_DOWN: 1-700
        7 monster_UP: 1-200
        8 monster_UP_RIGHT: 1-200
        9 monster_AHEAD: 1-200
        10 monster_DOWN_RIGHT: 1-200
        11 monster_DOWN: 1-200
        12 oxygen: 1-400        
          (see the specification file/manual for more details)
        :return: A Tuple containing the features you defined
        """
        
        # Considering the dimensions of the submarine bounding box
        water_max = 700
        water_min = 10
        ahead_max = 200
        ahead_min = 25
        upright_max = 200
        upright_min = sqrt(200)
        downright_max = 200
        downright_min = sqrt(450)

        sensors = self.sensors

        water_dis = sensors[0]

        # Water feature (0, 1)
        global STEPS_WATER
        water = 0
        if water_dis != 700:
            STEPS_WATER += 1
        else:
            STEPS_WATER = 0
        if STEPS_WATER > 10:
            water = 1

        #water45 = -sensors[1] + 700 + upright_min
        #water45 = normalize01(water45, lambda x: x, upright_min, upright_max)

        # Ahead, Upright and Downright features (0, 1)
        ahead = min(sensors[9], sensors[4])

        up = min(sensors[7], sensors[2])

        upright = min(sensors[8], sensors[3])

        down = min(sensors[11], sensors[6])

        downright = min(sensors[5], sensors[10])

        return (ahead, up, upright, down, downright, water_dis, water)

    def discretize_features(self, features: Tuple) -> Tuple:
        """
        This function should map the (possibly continuous) features (calculated by compute features) and discretize them.
        :param features 
        :return: A tuple containing the discretized features
        """
        features = list(features)
        for i in DISTANCES:
            features[i] = self.discretize_distance(features[i])

        features[WATER_DIS] = round((features[5]/700)*6)

        return tuple(features)

    @staticmethod
    def discretization_levels() -> Tuple:
        """
        This function should return a vector specifying how many discretization levels to use for each state feature.
        :return: A tuple containing the discretization levels of each feature
        """
        return (4, 4, 4, 4, 4, 7, 2)

    def get_current_state(self):
        """
        :return: computes the discretized features associated with this state object.
        """
        features = self.discretize_features(self.compute_features())
        return features

    @staticmethod
    def get_state_id(discretized_features: Tuple) -> int:
        """
        Handy function that calculates an unique integer identifier associated with the discretized state passed as
        parameter.
        :param discretized_features
        :return: unique key
        """

        terms = [primes[i] ** discretized_features[i] for i in range(len(discretized_features))]

        s_id = 1
        for i in range(len(discretized_features)):
            s_id = s_id * terms[i]

        return s_id

    @staticmethod
    def get_number_of_states() -> int:
        """
        Handy function that computes the total number of possible states that exist in the system, according to the
        discretization levels specified by the user.
        :return:
        """
        v = State.discretization_levels()
        num = 1

        for i in (v):
            num *= i

        return num

    @staticmethod
    def enumerate_all_possible_states() -> List:
        """
        Handy function that generates a list with all possible states of the system.
        :return: List with all possible states
        """
        levels = State.discretization_levels()
        levels_possibilities = [(j for j in range(i)) for i in levels]
        return [i for i in product(*levels_possibilities)]

    @staticmethod
    def discretize_distance(dist) -> int:
        if dist <= 60:
            return 0
        elif dist <= 120:
            return 1
        elif dist <= 165:
            return 2
        else:
            return 3


class QTable(controller_template.QTable):
    def __init__(self):
        """
        This class is used to create/load/store your Q-table. To store values we strongly recommend the use of a Python
        dictionary.
        """
        self.q_table = dict()
        for state in State.enumerate_all_possible_states():
            self.q_table[State.get_state_id(state)] = {ACTION_UP: -1, ACTION_DOWN: 0}

    def get_q_value(self, key: State, action: int) -> float:
        """
        Used to securely access the values within this q-table
        :param key: a State object 
        :param action: an action
        :return: The Q-value associated with the given state/action pair
        """
        return self.q_table[State.get_state_id(key.get_current_state())][action]

    def set_q_value(self, key: State, action: int, new_q_value: float) -> None:
        """
        Used to securely set the values within this q-table
        :param key: a State object 
        :param action: an action
        :param new_q_value: the new Q-value to associate with the specified state/action pair
        :return: 
        """
        self.q_table[State.get_state_id(key.get_current_state())][action] = new_q_value

    def save(self, path: str, *args) -> None:
        """
        This method must save this QTable to disk in the file file specified by 'path'
        :param path: 
        :param args: Any optional args you may find relevant; beware that they are optional and the function must work
                     properly without them.
        """
        with open(path, 'w+') as f:
            for key in self.q_table.keys():
                f.write(str(key) + " " + str(self.q_table[key][ACTION_UP]) + " " + str(self.q_table[key][ACTION_DOWN]) + "\n")


    @staticmethod
    def load(path: str) -> "QTable":
        """
        This method should load a Q-table from the specified file and return a corresponding QTable object
        :param path: path to file
        :return: a QTable object
        """
        q_table = QTable()
        with open(path, 'r') as f:
            for line in f.readlines():
                key, action1_value, action2_value = line.split()
                q_table.q_table[int(key)][ACTION_UP] = float(action1_value)
                q_table.q_table[int(key)][ACTION_DOWN] = float(action2_value)

        return q_table


class Controller(controller_template.Controller):
    def __init__(self, q_table_path: str):
        if q_table_path is None:
            self.q_table = QTable()
        else:
            self.q_table = QTable.load(q_table_path)

        self.episode_number = 0
        self.alpha = 0.9
        self.gamma = 0.995
        self.epsilon = 1
        self.decay = 1
        self.temperature = 0.01
        self.min_temperature = 0
        self.exploration = 'E-GREEDY'
        #$self.exploration = 'BOLTZMANN'

    def compute_reward(self, new_state: State, old_state: State, action: int, n_steps: int,
                       end_of_episode: bool) -> float:
        """
        This method is called by the learn() method in simulator.Simulation() to calculate the reward to be given to the agent
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get in new_state
        :param n_steps: number of steps the sub has taken so far in the current race
        :param end_of_episode: boolean indicating if an episode has ended
        :return: The reward to be given to the agent
        """
        old = old_state.get_current_state()
        new = new_state.get_current_state()

        reward = 0.0

        if old[WATER] == 1 and old_state.sensors[WATER_DIS] < new_state.sensors[WATER_DIS]:
            reward += 50

        if old[WATER] == 1 and old_state.sensors[WATER_DIS] > new_state.sensors[WATER_DIS]:
            reward -= 50

        for i in DISTANCES:
            reward += (new[i] - old[i])

        if old[WATER] == 1 and old[UP] == 0 and action == ACTION_UP:
            print('paçoca')
            reward -= 50

        if old[WATER] == 1 and old[UP] == 0 and action == ACTION_DOWN:
            print('paçoca')
            reward += 50

        if old[WATER] == 1 and new[AHEAD] < 3 and action == ACTION_UP:
            reward -= 50

        if old[WATER] == 1 and new[AHEAD] < 3 and action == ACTION_DOWN:
            reward += 50

        if old == (0, 1, 0, 3, 3, 6, 0) and action == ACTION_UP:
            reward -= 50

        print('a=', action, 'r=', reward, 's=', old_state.get_current_state(), 'qup=', self.q_table.get_q_value(old_state,1), 'qdown=', self.q_table.get_q_value(old_state,2))
        return reward

    def update_q(self, new_state: State, old_state: State, action: int, reward: float, end_of_race: bool) -> None:
        """
        This method is called by the learn() method in simulator.Simulation() to update your Q-table after each action is taken
        :param new_state: The state the sub just entered
        :param old_state: The state the sub just left
        :param action: the action the sub performed to get to new_state
        :param reward: the reward the sub received for getting to new_state  
        :param end_of_episode: boolean indicating if an episode has ended
        """
        q_value = (1 - self.alpha)*self.q_table.get_q_value(old_state, action) + self.alpha*(reward + self.gamma*max(self.q_table.get_q_value(new_state, a) for a in ACTIONS))

        self.q_table.set_q_value(old_state, action, q_value)

    def take_action(self, new_state: State, episode_number: int) -> int:
        """
        Decides which action the submarine must execute based on its Q-Table and on its exploration policy
        :param new_state: The current state of the submarine 
        :param episode_number: current episode/race during the training period
        :return: The action the submarine chooses to execute
        """
        if self.exploration == 'E-GREEDY':

            if random.random() < self.epsilon:
                action = random.randint(1, 2)

            else:
                if self.q_table.get_q_value(new_state, ACTION_UP) >= self.q_table.get_q_value(new_state, ACTION_DOWN):
                    action = ACTION_UP
                else:
                    action = ACTION_DOWN

            if episode_number > self.episode_number:
                self.epsilon = float(input())/10
                self.episode_number = episode_number
                self.epsilon *= self.decay
                print(self.epsilon)

        elif self.exploration == 'BOLTZMANN':

            q_up = norm01(self.q_table.get_q_value(new_state, 1), -5, 5)
            q_down = norm01(self.q_table.get_q_value(new_state, 2), -5, 5)

            prob_up = exp(q_up/self.temperature)/(exp(q_up/self.temperature) + exp(q_down/self.temperature))

            if random.random() < prob_up:
                action = ACTION_UP
            else:
                action = ACTION_DOWN

            if episode_number > self.episode_number:
                self.episode_number = episode_number
                if self.temperature > self.min_temperature:
                    self.temperature *= self.decay
                    #print(prob_up, q_up, q_down, self.temperature)
                else:
                    if self.q_table.get_q_value(new_state, 1) > self.q_table.get_q_value(new_state, 2):
                        action = ACTION_UP
                    else:
                        action = ACTION_DOWN

        return action


def norm01(x, a, b):
    return (x - a)/(b - a)